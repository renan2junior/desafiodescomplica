import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	text_title: {
		fontSize: 40,
		color: 'yellow',
		textAlign: 'center',
		margin: 20,
		backgroundColor: 'black'
	},
	view_ressurrect: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	}
});
