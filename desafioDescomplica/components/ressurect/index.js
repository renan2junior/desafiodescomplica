import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import styles from './styles';

export default ressurect = props => {
	return (
		<View style={styles.view_ressurrect}>
			<Text style={styles.text_title}>All jedi is dead!</Text>
			<Button
				iconRight
				title='Resurrect'
				onPress={() => props.action()}
				icon={
					<Icon
						name='refresh'
						type='SimpleLineIcons'
						size={25}
						color='white'
						style={{ marginLeft: 30 }}
					/>
				}
			/>
		</View>
	);
};
