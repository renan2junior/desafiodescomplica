import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	view_content: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'red'
	},
	view_input: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: 'black'
	},
	view_buttons: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: 'black'
	},
	view_result: {
		flex: 8,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'green',
		padding: 10
	},
	input_frase: {
		fontSize: 25,
		margin: (0, 0, 0, 10),
		backgroundColor: 'white'
	},
	scrollview_result: {
		flex: 1,
		backgroundColor: 'black',
		width: '100%',
		height: '100%'
	},
	view_box_result: {
		flex: 1,
		alignItems: 'center',
		padding: 20
	},
	text_result: {
		color: 'yellow',
		fontSize: 40
	},
	headerTitleStyle: {
		flex: 1,
		textAlign: 'center',
		fontSize: 20,
		color: 'black'
	}
});
