import React from 'react';
import { View, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Button } from 'react-native-elements';
import Personagem from '../personagem';
import styles from './styles';

export default jedis = props => {
	return (
		<View>
			<FlatList
				data={props.list}
				keyExtractor={item => item.name}
				renderItem={({ item }) => (
					<View style={styles.view_personagem}>
						<Personagem cor={item.eye_color.split('-')[0]} name={item.name} />
						<Button
							iconRight
							onPress={() => props.action(item)}
							icon={
								<Icon
									name='trash'
									size={25}
									color='white'
									style={{ padding: 2 }}
								/>
							}
						/>
					</View>
				)}
			/>
		</View>
	);
};
