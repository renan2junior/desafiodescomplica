import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import TabBarIcon from '../components/TabBarIcon';
import Tela1Screen from '../screens/teste1';
import Tela2Screen from '../screens/teste2';

const Tela1Stack = createStackNavigator({
	Tela1: Tela1Screen
});

Tela1Stack.navigationOptions = {
	tabBarLabel: 'Test 1',
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'}
		/>
	)
};

const Tela2Stack = createStackNavigator({
	Tela2: Tela2Screen
});

Tela2Stack.navigationOptions = {
	tabBarLabel: 'Test 2',
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'}
		/>
	)
};

const tabNavigator = createBottomTabNavigator(
	{
		Tela1Stack,
		Tela2Stack
	},

	{
		tabBarOptions: {
			showLabel: true,
			activeTintColor: '#F8F8F8',
			inactiveTintColor: '#586589',
			style: {
				backgroundColor: '#171F33'
			}
		}
	}
);

export default tabNavigator;
