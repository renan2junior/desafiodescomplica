import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	view_personagem: {
		flex: 1,
		flexDirection: 'row',
		padding: 20
	}
});
