export const api = callback => {
	fetch('https://swapi.co/api/people', {
		method: 'GET'
	})
		.then(response => response.json())
		.then(responseJson => {
			return callback(responseJson.results);
		});
};
