import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const Personagem = props => {
	return (
		<View style={{ flex: 1 }}>
			<Text style={[styles.text_name, { color: props.cor }]}>{props.name}</Text>
		</View>
	);
};

export default Personagem;
