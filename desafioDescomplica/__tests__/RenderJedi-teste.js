import React from 'react';
import renderer from 'react-test-renderer';
import Jedis from '../components/jedis';
import { api } from '../util/api';

beforeEach(() => {
	fetch.resetMocks();
});

it(`Testando numero de itens`, () => {
	const onResponse = jest.fn();
	api(rs => {
		const persons = renderer.create(<Jedis list={rs} />).toJSON();
		expect(persons).toMatchSnapshot();
	});
});
