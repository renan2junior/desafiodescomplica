import React, { Component } from 'react';
import { Text, View, TextInput, ScrollView } from 'react-native';
import { Button } from 'react-native-elements';
import styles from './styles';

export default class Tela1Screen extends Component {
	static navigationOptions = {
		title: 'Test 1',
		headerStyle: {
			backgroundColor: 'yellow'
		}
	};
	constructor(props) {
		super(props);
		this.state = {
			list_word: {},
			frase: 'Vim,vi e venci'
		};
	}

	render() {
		return (
			<View style={styles.view_content}>
				<View style={styles.view_input}>
					<TextInput
						style={styles.input_frase}
						autoCompleteType={'off'}
						autoCorrect={false}
						placeholder='write a phrase.'
						value={this.state.frase}
						onChangeText={text => {
							this.setState({ frase: text });
						}}
					/>
				</View>
				<View style={styles.view_buttons}>
					<Button onPress={() => this.orderListNames()} title='COUNT' />
					<Button onPress={() => this.clearList()} title='CLEAR' />
				</View>
				<View style={styles.view_result}>
					<ScrollView style={styles.scrollview_result}>
						<View style={styles.view_box_result}>
							<Text style={styles.text_result}>
								{JSON.stringify(this.state.list_word, null, '\t')}
							</Text>
						</View>
					</ScrollView>
				</View>
			</View>
		);
	}

	clearList = () => {
		this.setState({ list_word: {}, frase: '' });
	};

	orderListNames = () => {
		let list_word = this.state.frase.replace(/[^A-Za-z0-9_]/g, ' ').split(' ');
		let list_count = {};
		for (var i in list_word) {
			list_count[list_word[i]] = (list_count[list_word[i]] || 0) + 1;
		}
		this.setState({ list_word: list_count });
	};
}
