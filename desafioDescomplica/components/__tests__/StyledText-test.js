import React from 'react';
import renderer from 'react-test-renderer';
import Jedis from '../jedis';
import { api } from '../../util/api';

import { MonoText } from '../StyledText';

it(`renders correctly`, () => {
	const tree = renderer.create(<MonoText>Snapshot test!</MonoText>).toJSON();

	expect(tree).toMatchSnapshot();
});

beforeEach(() => {
	console.log('aqui');
	fetch.resetMocks();
	res = [
		{
			name: 'Biggs Darklighter',
			height: '183',
			mass: '84',
			hair_color: 'black',
			skin_color: 'light',
			eye_color: 'brown'
		},
		{
			name: 'R5-D4',
			height: '97',
			mass: '32',
			hair_color: 'n/a',
			skin_color: 'white',
			eye_color: 'red'
		}
	];
	api(r => {
		res = r;
		console.log(res);
	});
});

it(`Rqestando numero de itens agora`, async () => {
	const persons = renderer
		.create(
			<Jedis
				list={res}
				action={() => {
					false;
				}}
			/>
		)
		.toJSON();
	expect(persons).toMatchSnapshot();
});
