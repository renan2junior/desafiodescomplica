import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	text_title: {
		fontSize: 40,
		color: 'yellow',
		textAlign: 'center',
		margin: 20,
		backgroundColor: '#000000'
	},
	view_content: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'red',
		backgroundColor: '#000000'
	},
	view_personagem: {
		flex: 1,
		flexDirection: 'row',
		padding: 20
	},
	headerTitleStyle: {
		flex: 1,
		textAlign: 'center',
		fontSize: 20,
		color: '#ffff00'
	},
	view_ressurrect: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	}
});
