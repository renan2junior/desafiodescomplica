import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	text_name: {
		fontSize: 30,
		textAlign: 'center',
		color: 'white'
	}
});
