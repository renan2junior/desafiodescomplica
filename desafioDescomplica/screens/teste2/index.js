import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import Ressurect from '../../components/ressurect';
import Jedis from '../../components/jedis';
import styles from './styles';
import { api } from '../../util/api';

export default class Tela2Screen extends Component {
	static navigationOptions = {
		title: 'Test 2',
		headerStyle: {
			backgroundColor: 'yellow'
		}
	};
	constructor(props) {
		super(props);
		this.state = { list_jedi: [], load: true };
	}

	componentDidMount() {
		this.getDados();
	}

	render() {
		return (
			<View style={styles.view_content}>
				<Text style={styles.text_title}>StarWars Characters</Text>

				{!this.state.load ? (
					this.state.list_jedi.length < 1 ? (
						<Ressurect action={this.getDados} />
					) : (
						<Jedis list={this.state.list_jedi} action={this.removePerson} />
					)
				) : (
					<ActivityIndicator size='large' color='#00ff00' />
				)}
			</View>
		);
	}

	removePerson = person => {
		var x = this.state.list_jedi;
		x = x.filter(item => {
			return item.name != person.name;
		});
		this.setState({
			list_jedi: this.state.list_jedi.filter(item => item.name != person.name)
		});
	};

	getDados = () => {
		this.setState({ load: true });
		api(rs => {
			this.setState({
				list_jedi: rs,
				load: false
			});
			this.orderList();
		});
	};

	orderList = () => {
		this.setState({
			ist_jedi: this.state.list_jedi.sort((a, b) => {
				if (a.name.toUpperCase() > b.name.toUpperCase()) {
					return 0;
				}
				return -1;
			})
		});
	};
}
